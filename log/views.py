# coding: utf-8
from django.shortcuts import render, get_object_or_404
from django.http import Http404


from log.models import Log


def index(request):
    context = {'logs': Log.objects.all()}
    return render(request, 'log/index.jinja', context)


def detail(request, pk):
    log = get_object_or_404(Log, pk=pk)
    inventory = log.inventories.all() if log else None
    context = {'log': log, 'inventory': inventory}
    return render(request, 'log/detail.jinja', context)
