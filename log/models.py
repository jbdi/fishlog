# coding: utf-8
from django.db import models
from django.conf import settings
from djrichtextfield.models import RichTextField
from jsonfield import JSONField


class InventoryItem(models.Model):
    # статус проверки модератором?
    # предлагать автокомплитом что-нибудь
    title = models.CharField(max_length=250, null=False, default='')

    def __unicode__(self):
        return self.title


class Inventory(models.Model):
    """
    замутить целостность структуры с инвентарем
    если удаляем элемент инвентаря, то что делать?
    """
    inventory_item = models.ManyToManyField('InventoryItem')
    user = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='inventories', null=True) # NOTE: remove null=True
    date = models.DateTimeField(auto_now=True, null=True) # NOTE: remove null=True
    log = models.ForeignKey('Log', related_name='inventories', null=True)

    def __unicode__(self):
        return 'set_id: {}'.format(self.pk)


class Log(models.Model):
    """
    сеанс рыбалки - это запись в журнале, время рыбалки с привязкой к месту, используемому инвентарю, улову, погодными условиями
    """
    date = models.DateTimeField(auto_now=True)
    enviroment = models.ForeignKey('EnviromentData', blank=True, null=True)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='log_items', default=None)
    catch = models.ForeignKey('Catch', related_name='log_items', default=None)
    # inventory = models.ForeignKey('Inventory', related_name='log_items', default=None, null=True)

    def __unicode__(self):
        return u"{}".format(self.catch)

    class Meta:
        ordering = ('-date',)



class Catch(models.Model):
    video_url = models.URLField() # make validation for only youtube
    description = RichTextField()
    user = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='fishing_items', default=None)
    weight = models.PositiveSmallIntegerField(null=False, blank=True)
    length = models.PositiveSmallIntegerField(null=False, blank=True)
    subject = models.ForeignKey('FishCatalog', related_name='catches', default=None)

    # method = способ вылова рыбы
    # bait = какая приманка (в fishbrain квадриллиард их)
    # release = отпустил
    # enviroment
    # comments

    def photos(self):
        pass
        # return CatchPhoto.objects.filter(user_id_link=self.user.id, item=self)

    def __unicode__(self):
        return u'{}, {}гр, {}см'. format(self.subject, self.weight, self.length)


class CatchPhoto(models.Model):
    """
    NOTE: возможно, стоит сделать из класса фабрику,
          чтобы другие маодели могли юзать множество фоток
    """
    item = models.ForeignKey('Catch', on_delete=models.CASCADE, related_name='photos')
    photo = models.ImageField() # NOTE: можно создавать папки под каждого юзера
    user_id_link = models.PositiveIntegerField()


class EnviromentData(models.Model):
    # NOTE: скорее всего для гео полей, в будущем, надо будет подключить geo_django для вычисления каких-нибудь полигонов. Но пока что это не надо и для обычных точек на карте можно использовать одно поле с координатами
    # geo_lattitude
    # geo_longtitude
    # user_id
    # label or tag [метка источника данных, например, из какой модели пришли данные или из урл]
    datetime = models.DateTimeField(auto_now=True)
    temperature = models.SmallIntegerField(null=False, blank=True)
    # узнать популярные единицы измерения давления
    # http://tehtab.ru/guide/guideunitsalphabets/guideunitsalphabets/pressurevacuumfile/pressureconvertiontable/
    pressure = models.SmallIntegerField(null=False, blank=True)
    # влажность в процентах
    humidity = models.PositiveSmallIntegerField(null=False, blank=True)
    # moon_phase



############




class FishCatalog(models.Model):
    """
    TODO: нужно добавить привязку рыб к водоемам
    Добавить дополнительную информацию о рыбе (отряд, семейство, род)
    """
    name = models.CharField(max_length=250, default='')
    description = RichTextField(default='')
    photo = models.ImageField(null=True, default=None)

    def __unicode__(self):
        return self.name

class GeoPoints(models.Model):
    pass

class BaitTypes(models.Model):
    name = models.CharField(max_length=250, default='')
    description = RichTextField(default='', null=True)

    def __unicode__(self):
        return self.name

class Bait(models.Model):
    name = models.CharField(max_length=250, default='')
    type = models.ForeignKey(BaitTypes)
    photo = models.ImageField(null=True, default=None)
    description = RichTextField(default='', null=True)
    parameters = JSONField()

    def __unicode__(self):
        return self.name
