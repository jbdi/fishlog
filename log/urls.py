# from django.conf import settings
from django.conf.urls import patterns, include, url


urlpatterns = patterns('',
    url(r'log/$', 'log.views.index', name='log_index'),
    url(r'log/(?P<pk>[\d\w\_]+)$', 'log.views.detail', name='log_detail'),

)