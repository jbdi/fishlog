# coding: utf-8
from django.contrib import admin

from log.models import (InventoryItem, Inventory, FishCatalog,
    Catch, CatchPhoto, BaitTypes, Bait, Log, EnviromentData,)


@admin.register(CatchPhoto)
class CatchPhotoAdmin(admin.ModelAdmin):
    pass


@admin.register(Catch)
class CatchAdmin(admin.ModelAdmin):
    pass


@admin.register(EnviromentData)
class EnviromentDataAdmin(admin.ModelAdmin):
    pass


@admin.register(Log)
class LogAdmin(admin.ModelAdmin):
    pass


@admin.register(InventoryItem)
class InventoryItemAdmin(admin.ModelAdmin):
    pass


@admin.register(Inventory)
class InventoryAdmin(admin.ModelAdmin):
    pass


@admin.register(FishCatalog)
class FishCatalogAdmin(admin.ModelAdmin):
    pass


@admin.register(BaitTypes)
class BaitTypesAdmin(admin.ModelAdmin):
    pass


@admin.register(Bait)
class BaitAdmin(admin.ModelAdmin):
    pass
