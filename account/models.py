# coding: utf-8
from django.db import models
from django.contrib.auth.models import (BaseUserManager, AbstractUser)
from django.conf import settings

from django.utils import timezone


class UserManager(BaseUserManager):

    def _create_user(self, username, email, password,
                     is_staff, is_superuser, **extra_fields):
        """
        Creates and saves a User with the given username, email and password.
        """
        now = timezone.now()
        if not username:
            raise ValueError('The given username must be set')
        email = self.normalize_email(email)
        profile = UserProfile()
        profile.save(using=self._db) # профиль нужно создавать пустым, но заполнять данными из формы
        user = self.model(username=username, email=email,
                          is_staff=is_staff, is_active=True,
                          is_superuser=is_superuser, last_login=now,
                          date_joined=now, profile=profile, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, username, email=None, password=None, **extra_fields):
        return self._create_user(username, email, password, False, False,
                                 **extra_fields)

    def create_superuser(self, username, email, password, **extra_fields):
        return self._create_user(username, email, password, True, True,
                                 **extra_fields)


class UserProfile(models.Model):
    pass
    # inventory
    # analytics
    # fishing


class User(AbstractUser):
    profile = models.ForeignKey(UserProfile, on_delete=models.CASCADE, related_name='users', null=False)
    objects = UserManager()



